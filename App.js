const Map = L.map('Map').setView([0, 0], 3);
const Geocoder = L.esri.Geocoding.geocodeService();
const OverpassURL = 'https://overpass-api.de/api/interpreter?data=';

var ClickPopup = L.popup();

var Point0, Point1, Point0Marker, Point1Marker, GPSDot, GeoCircle, GeoSquare;
var [ZoomedAfterPosition, MarkerFromGPS, GotGPSPermission] = [false, false, false];
var [PathDistance, PathRadius] = [DistanceInput.valueAsNumber, RadiusInput.valueAsNumber];

// https://medium.com/@haliskaya/the-trick-to-viewport-units-on-mobile-218f83ed12e1
function ViewportResize() {
	let vh = window.innerHeight * 0.01;
	document.documentElement.style.setProperty('--vh', `${vh}px`);
}
ViewportResize();
window.addEventListener('resize', ViewportResize);

const Tiles = {
	"OpenStreetMap": L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
		maxZoom: 19,
		attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
	}),
};
var TileSelected = Tiles["OpenStreetMap"].addTo(Map);

DistanceInput.oninput = function() {
	PathDistance = this.valueAsNumber;
}

RadiusInput.oninput = function() {
	PathRadius = this.valueAsNumber;
}

function RemoveFromMap(Item) {
	if (Item != null)
		Item.removeFrom(Map);
}

function MarkerAdd(GeoPoint) {
	return L.marker(GeoPoint).addTo(Map);
}
function MarkerPlace(GeoPoint) {
	RemoveFromMap(Point0Marker);
	Point0 = GeoPoint;
	Point0Marker = MarkerAdd(Point0);
	StartingPoint.innerHTML = "Starting point: " + Point0;
}

function DotAdd(GeoPoint, Acc) {
	return L.circleMarker(GeoPoint, {radius: 5, fillOpacity: 100});
}
function DotPlace(GeoPoint, Acc) {
	RemoveFromMap(GPSDot);
	GPSDot = DotAdd(GeoPoint, Acc);
}

function SquareAdd(GeoBox) {
	return L.polygon(GeoBox).addTo(Map);
}
function SquarePlace(GeoBox) {
	RemoveFromMap(GeoSquare);
	GeoSquare = SquareAdd(GeoBox);
}

function CircleAdd(GeoPoint, Acc) {
	return L.circle(GeoPoint, {radius: Acc});
}
function CirclePlace(GeoPoint, Acc) {
	RemoveFromMap(GeoCircle);
	GeoCircle = CircleAdd(GeoPoint, Acc);
}

function MapSelectClean() {
	StopGPS();
	NavigationToggle.state('add-markers');
	RemoveFromMap(GeoCircle);
	RemoveFromMap(GeoSquare);
}

function GeocoderMarkerPlace(Coords) {
	Geocoder.reverse().latlng(Coords).run(function(err, res) {
		console.log(res.address.City);
		MarkerPlace(res.latlng);
	});
}

function OnMapClick(e) {
	MapSelectClean();
	MarkerPlace(e.latlng);
	GeocoderMarkerPlace(e.latlng);
}
Map.on('click', OnMapClick);

L.Control.geocoder({
	defaultMarkGeocode: false
})
.on('markgeocode', function(e) {
	MapSelectClean();

	let Box = e.geocode.bbox;
	let GeoBox = [Box.getSouthEast(), Box.getNorthEast(), Box.getNorthWest(), Box.getSouthWest()];
	SquarePlace(GeoBox);
	MarkerPlace(e.geocode.center);

	Map.fitBounds(GeoSquare.getBounds());
}).addTo(Map);

function ShowGPSPosition(Coords, Acc) {
	CirclePlace(Coords, Acc);
	DotPlace(Coords, Acc);

	let featureGroup = L.featureGroup([GPSDot, GeoCircle]).addTo(Map);

	if (!ZoomedAfterPosition) {
		Map.fitBounds(featureGroup.getBounds());
		ZoomedAfterPosition = true;
	}
}

var GPSWatch;
const GPSOpt = {enableHighAccuracy: true, timeout: 5000, maximumAge: 0};

function GPSOk(GPSData) {
	if (MarkerFromGPS) {
		let Coords = [GPSData.coords.latitude, GPSData.coords.longitude];
		let Acc = GPSData.coords.accuracy;

		Point0 = Coords;
		StartingPoint.innerHTML = "Starting point: " + Point0;
		ShowGPSPosition(Coords, Acc);
	}
}
function GPSErr(e) {
	console.warn('GPS Error: (' + e.code + '): ' + e.message);
}

function StartGPS() {
	MapSelectClean();
	MarkerFromGPS = true;
	if (!GotGPSPermission) {
		GPSWatch = navigator.geolocation.watchPosition(GPSOk, GPSErr, GPSOpt);
		GotGPSPermission = true;
	}
}
function StopGPS() {
	[ZoomedAfterPosition, MarkerFromGPS] = [false, false];
}

var NavigationToggle = L.easyButton({
	states: [{
		stateName: 'add-markers',
		icon: 'fa-map-marker',
		title: 'Start GPS navigation',
		onClick: function(Control) {
			StartGPS();
			Control.state('remove-markers');
		}
	}, {
		icon: 'fa-location-arrow',
		stateName: 'remove-markers',
		onClick: function(Control) {
			StopGPS();
			Control.state('add-markers');
		},
		title: 'Stop GPS navigation'
	}]
}).addTo(Map);

function FetchJson(Url) {
	try {
		return fetch(Url).then(response => response.json());
	} catch(e) {
		console.warn('Fetch Error: (' + e.code + '): ' + e.message);
	}
}

function RandomPathGen() {
	let Data = '[out:json];out;';
	let Url = OverpassURL + Data;
	
	let Res = FetchJson(Url).then(function(Res) {
		console.log(Res);
		return Res;
	});
}
